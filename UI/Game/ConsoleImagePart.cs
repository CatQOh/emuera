﻿using MinorShift.Emuera.Runtime.Config;
using MinorShift.Emuera.UI.Game.Image;
using System;
using System.Drawing;
using System.Text;
namespace MinorShift.Emuera.UI.Game;

sealed class ConsoleImagePart : AConsoleDisplayPart
{

    public ConsoleImagePart(string resName, string resNameb, int raw_height, int raw_width, int raw_ypos, bool usePxWidth = false, bool usePxHeight = false, DisplayMode display = DisplayMode.Relative)
    {
        top = 0;
        bottom = Config.FontSize;
        Str = "";
        ResourceName = resName ?? "";
        ButtonResourceName = resNameb;
        StringBuilder sb = new();
        sb.Append("<img src='");
        sb.Append(ResourceName);
        if (ButtonResourceName != null)
        {
            sb.Append("' srcb='");
            sb.Append(ButtonResourceName);
        }
        if (raw_height != 0)
        {
            sb.Append("' height='");
            sb.Append(raw_height);
        }
        if (raw_width != 0)
        {
            sb.Append("' width='");
            sb.Append(raw_width);
        }
        if (raw_ypos != 0)
        {
            sb.Append("' ypos='");
            sb.Append(raw_ypos);
        }
        sb.Append("'>");
        AltText = sb.ToString();
        cImage = AppContents.GetSprite(ResourceName);
        //if (cImage != null && !cImage.IsCreated)
        //	cImage = null;
        if (cImage == null)
        {
            Str = AltText;
            return;
        }
        int height;

        if (raw_height == 0)
        {
            //HTMLで高さが指定されていない又は0が指定された場合、フォントサイズをそのまま高さ(px単位)として使用する。
            height = Config.FontSize;
        }
        else
        {
            if (usePxHeight)
            {
                height = raw_height;
            }
            else
            {
                //HTMLで高さが指定された場合、フォントサイズの100分率と解釈する。
                height = Config.FontSize * raw_height / 100;
            }
        }
        //幅が指定されていない又は0が指定された場合、元画像の縦横比を維持するように幅(px単位)を設定する。1未満は端数としてXsubpixelに記録。
        //負の値が指定される可能性があるが、最終的なWidthは正の値になるようにあとで調整する。
        if (raw_width == 0)
        {
            Width = cImage.DestBaseSize.Width * height / cImage.DestBaseSize.Height;
            XsubPixel = (float)cImage.DestBaseSize.Width * height / cImage.DestBaseSize.Height - Width;
        }
        else
        {
            if (usePxWidth)
            {
                Width = raw_width;
            }
            else
            {
                Width = Config.FontSize * raw_width / 100;
            }
            XsubPixel = (float)Config.FontSize * raw_width / 100f - Width;
        }
        top = raw_ypos * Config.FontSize / 100;
        destRect = new Rectangle(0, top, Width, height);
        if (destRect.Width < 0)
        {
            destRect.X = -destRect.Width;
            Width = -destRect.Width;
        }
        if (destRect.Height < 0)
        {
            destRect.Y = destRect.Y - destRect.Height;
            height = -destRect.Height;
        }
        bottom = top + height;
        //if(top > 0)
        //	top = 0;
        //if(bottom < Config.FontSize)
        //	bottom = Config.FontSize;
        if (ButtonResourceName != null)
        {
            cImageB = AppContents.GetSprite(ButtonResourceName);
            //if (cImageB != null && !cImageB.IsCreated)
            //	cImageB = null;
        }

        _display = display;
    }

    private readonly ASprite cImage;
    private readonly ASprite cImageB;
    private readonly int top;
    private readonly int bottom;
    private readonly Rectangle destRect;
    //#pragma warning disable CS0649 // フィールド 'ConsoleImagePart.ia' は割り当てられません。常に既定値 null を使用します。
    //		private readonly ImageAttributes ia;
    //#pragma warning restore CS0649 // フィールド 'ConsoleImagePart.ia' は割り当てられません。常に既定値 null を使用します。
    public readonly string ResourceName;
    public readonly string ButtonResourceName;
    public override int Top { get { return top; } }
    public override int Bottom { get { return bottom; } }

    DisplayMode _display;

    public override bool CanDivide { get { return false; } }
    public override void SetWidth(StringMeasure sm, float subPixel)
    {
        if (Error)
        {
            Width = 0;
            return;
        }
        if (cImage != null)
            return;
        Width = sm.GetDisplayLength(Str, Config.DefaultFont);
        XsubPixel = subPixel;
    }

    public override string ToString()
    {
        if (AltText == null)
            return "";
        return AltText;
    }

    public override void DrawTo(Graphics graph, int pointY, bool isSelecting, bool isBackLog, TextDrawingMode mode, bool isButton = false)
    {
        if (Error)
            return;
        ASprite img = cImage;
        if (isSelecting && cImageB != null)
            img = cImageB;

        if (img != null && img.IsCreated)
        {
            Rectangle rect = destRect;
            //PointX微調整
            switch (_display)
            {
                case DisplayMode.Relative:
                    rect.X = destRect.X + PointX + Config.DrawingParam_ShapePositionShift;
                    rect.Y = destRect.Y + pointY;
                    break;
                case DisplayMode.AbsoluteLeftTop:
                    rect.X = destRect.X;
                    rect.Y = destRect.Y;
                    break;
                default:
                    throw new NotImplementedException();
            }
            img.GraphicsDraw(graph, rect);
        }
        else
        {
            var point = new Point(PointX, pointY);
            if (mode == TextDrawingMode.GRAPHICS)
                graph.DrawString(AltText, Config.DefaultFont, new SolidBrush(Config.ForeColor), point);
            else
                System.Windows.Forms.TextRenderer.DrawText(graph, AltText.AsSpan(), Config.DefaultFont, point, Config.ForeColor, System.Windows.Forms.TextFormatFlags.NoPrefix);
        }
    }
}
