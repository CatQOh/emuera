﻿using MinorShift.Emuera.GameView;
using MinorShift.Emuera.Runtime.Config;
using MinorShift.Emuera.Runtime.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace MinorShift.Emuera.UI.Game;

/// <summary>
/// ボタン。1つ以上の装飾付文字列（ConsoleStyledString）からなる。
/// </summary>
internal sealed class ConsoleButtonString
{
    public ConsoleButtonString(EmueraConsole console, AConsoleDisplayPart[] strs)
    {
        parent = console;
        strArray = strs;
        IsButton = false;
        PointX = -1;
        Width = -1;
        ErrPos = null;
    }
    public ConsoleButtonString(EmueraConsole console, AConsoleDisplayPart[] strs, long input)
        : this(console, strs)
    {
        Input = input;
        Inputs = input.ToString();
        IsButton = true;
        IsInteger = true;
        if (console != null)
        {
            Generation = parent.NewButtonGeneration;
            console.UpdateGeneration();
        }
        ErrPos = null;
    }
    public ConsoleButtonString(EmueraConsole console, AConsoleDisplayPart[] strs, string inputs)
        : this(console, strs)
    {
        Inputs = inputs;
        IsButton = true;
        IsInteger = false;
        if (console != null)
        {
            Generation = parent.NewButtonGeneration;
            console.UpdateGeneration();
        }
        ErrPos = null;
    }

    public ConsoleButtonString(EmueraConsole console, AConsoleDisplayPart[] strs, long input, string inputs)
        : this(console, strs)
    {
        Input = input;
        Inputs = inputs;
        IsButton = true;
        IsInteger = true;
        if (console != null)
        {
            Generation = parent.NewButtonGeneration;
            console.UpdateGeneration();
        }
        ErrPos = null;
    }
    public ConsoleButtonString(EmueraConsole console, AConsoleDisplayPart[] strs, string inputs, ScriptPosition? pos)
        : this(console, strs)
    {
        Inputs = inputs;
        IsButton = true;
        IsInteger = false;
        if (console != null)
        {
            Generation = parent.NewButtonGeneration;
            console.UpdateGeneration();
        }
        ErrPos = pos;
    }

    AConsoleDisplayPart[] strArray;
    public AConsoleDisplayPart[] StrArray { get { return strArray; } }
    EmueraConsole parent;

    public ConsoleDisplayLine ParentLine { get; set; }
    public bool IsButton { get; private set; }
    public bool IsInteger { get; private set; }
    public long Input { get; private set; }
    public string Inputs { get; private set; }
    public int PointX { get; set; }
    public bool PointXisLocked { get; set; }
    public int Width { get; set; }
    public float XsubPixel { get; set; }
    public long Generation { get; private set; }
    public ScriptPosition? ErrPos { get; set; }
    public string Title { get; set; }


    public int RelativePointX { get; private set; }
    public void LockPointX(int rel_px)
    {
        PointX = rel_px * Config.FontSize / 100;
        XsubPixel = rel_px * Config.FontSize / 100.0f - PointX;
        PointXisLocked = true;
        RelativePointX = rel_px;
    }

    //indexの文字数の前方文字列とindex以降の後方文字列に分割
    public ConsoleButtonString DivideAt(int divIndex, StringMeasure sm)
    {
        if (divIndex <= 0)
            return null;
        List<AConsoleDisplayPart> cssListA = [];
        List<AConsoleDisplayPart> cssListB = [];
        int index = 0;
        int cssIndex;
        bool b = false;
        for (cssIndex = 0; cssIndex < strArray.Length; cssIndex++)
        {
            if (b)
            {
                cssListB.Add(strArray[cssIndex]);
                continue;
            }
            int length = strArray[cssIndex].Str.Length;
            if (divIndex < index + length)
            {
                ConsoleStyledString oldcss = strArray[cssIndex] as ConsoleStyledString;
                if (oldcss == null || !oldcss.CanDivide)
                    throw new ExeEE("文字列分割異常");
                ConsoleStyledString newCss = oldcss.DivideAt(divIndex - index, sm);
                cssListA.Add(oldcss);
                if (newCss != null)
                    cssListB.Add(newCss);
                b = true;
                continue;
            }
            else if (divIndex == index + length)
            {
                cssListA.Add(strArray[cssIndex]);
                b = true;
                continue;
            }
            index += length;
            cssListA.Add(strArray[cssIndex]);
        }
        if (cssIndex >= strArray.Length && cssListB.Count == 0)
            return null;
        AConsoleDisplayPart[] cssArrayA = new AConsoleDisplayPart[cssListA.Count];
        AConsoleDisplayPart[] cssArrayB = new AConsoleDisplayPart[cssListB.Count];
        cssListA.CopyTo(cssArrayA);
        cssListB.CopyTo(cssArrayB);
        strArray = cssArrayA;
        ConsoleButtonString ret = new(null, cssArrayB);
        CalcWidth(sm, XsubPixel);
        ret.CalcWidth(sm, 0);
        CalcPointX(PointX);
        ret.CalcPointX(PointX + Width);
        ret.parent = parent;
        ret.ParentLine = ParentLine;
        ret.IsButton = IsButton;
        ret.IsInteger = IsInteger;
        ret.Input = Input;
        ret.Inputs = Inputs;
        ret.Generation = Generation;
        ret.ErrPos = ErrPos;
        ret.Title = Title;
        return ret;
    }

    public void CalcWidth(StringMeasure sm, float subpixel)
    {
        Width = -1;
        if (strArray != null && strArray.Length > 0)
        {
            Width = 0;
            foreach (AConsoleDisplayPart css in strArray)
            {
                if (css.Width <= 0)
                    css.SetWidth(sm, subpixel);
                Width += css.Width;
                subpixel = css.XsubPixel;
            }
            if (Width <= 0)
                Width = -1;
        }
        XsubPixel = subpixel;
    }

    /// <summary>
    /// 先にCalcWidthすること。
    /// </summary>
    /// <param name="sm"></param>
    public void CalcPointX(int pointx)
    {
        int px = pointx;
        if (!PointXisLocked)
            PointX = px;
        else
            px = PointX;
        for (int i = 0; i < strArray.Length; i++)
        {
            strArray[i].PointX = px;
            px += strArray[i].Width;
        }
        if (strArray.Length > 0)
        {
            PointX = strArray[0].PointX;
            Width = strArray[^1].PointX + strArray[^1].Width - PointX;
            //if (Width < 0)
            //	Width = -1;
        }
    }

    internal void ShiftPositionX(int shiftX)
    {
        PointX += shiftX;
        foreach (AConsoleDisplayPart css in strArray)
            css.PointX += shiftX;
    }

    public void DrawTo(Graphics graph, int pointY, bool isBackLog, TextDrawingMode mode)
    {
        bool isSelecting = IsButton && parent.ButtonIsSelected(this);
        foreach (AConsoleDisplayPart css in strArray)
            css.DrawTo(graph, pointY, isSelecting, isBackLog, mode, IsButton);
    }

    readonly static StringBuilder builder = new();
    public override string ToString()
    {
        if (strArray == null)
            return "";
        builder.Clear();
        foreach (var css in strArray)
            builder.Append(css.ToString());
        return builder.ToString();
    }

}
